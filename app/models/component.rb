class Component < ActiveRecord::Base
    belongs_to :category
    belongs_to :distributor
    belongs_to :mount
    belongs_to :package
    belongs_to :moneda
    belongs_to :project
    belongs_to :place
    belongs_to :user

   validates_uniqueness_of :ref
end

class CreateComponents < ActiveRecord::Migration
  def change
    create_table :components do |t|
      #t.string :place
      t.integer :place_id, :default => 0
      t.integer :category_id, :default => 0
      t.string :description
      t.string :value
      t.integer :mount_id, :default => 0
      t.integer :package_id, :default => 0
      t.integer :distributor_id, :default => 0
      t.string :ref
      t.string :price
      t.integer :moneda_id, :default => 0
      t.string :comments
      t.integer :quantity
      t.integer :project_id, :default => 0
      t.datetime :date_registrer, :default => Time.at(0)
      t.datetime :date_submit, :default => Time.at(0)
      t.datetime :date_reception, :default => Time.at(0)
      t.datetime :date_check, :default => Time.at(0)
      #t.string :user, :default => ""
      t.integer :user_id, :default => 0

      t.timestamps
    end
  end
end

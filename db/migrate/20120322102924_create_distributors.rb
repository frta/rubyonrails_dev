class CreateDistributors < ActiveRecord::Migration
  def change
    create_table :distributors do |t|
      t.string :name

      t.timestamps
    end
      #Distributor.create :name => "Desconocido"
      Distributor.create :name => "Mouser"
      Distributor.create :name => "Digi-key"
      Distributor.create :name => "Rs-Online"
      Distributor.create :name => "Farnell"
  end
end

class CreatePackages < ActiveRecord::Migration
  def change
    create_table :packages do |t|
      t.string :name
      t.string :image_name

      t.timestamps
    end
      #Package.create :name => "Desconocido"
      Package.create :name => "0402"
      Package.create :name => "0603"
      Package.create :name => "0612"
      Package.create :name => "0805"
      Package.create :name => "1206"
      Package.create :name => "1218"
      Package.create :name => "2010"
      Package.create :name => "Axial"
      Package.create :name => "BGA"
      Package.create :name => "DDPAK"
      Package.create :name => "DIP"
      Package.create :name => "DPAK"
      Package.create :name => "FDIP"
      Package.create :name => "PDIP"
      Package.create :name => "PENTAWATT"
      Package.create :name => "PLCC"
      Package.create :name => "QDIP"
      Package.create :name => "QFP"
      Package.create :name => "Radial"
      Package.create :name => "SIP"
      Package.create :name => "SO"
      Package.create :name => "SO8"
      Package.create :name => "SOT223"
      Package.create :name => "SOT23"
      Package.create :name => "SQL"
      Package.create :name => "SQP"
      Package.create :name => "SW"
      Package.create :name => "T7-TO220"
      Package.create :name => "TO220"
      Package.create :name => "TO2205"
      Package.create :name => "TO220ISO"
      Package.create :name => "TO252"
      Package.create :name => "TO263"
      Package.create :name => "TO268"
      Package.create :name => "TO52"
      Package.create :name => "TO99"
      Package.create :name => "TSOP"
      Package.create :name => "ZIP"
  end
end
